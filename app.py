from flask import Flask, render_template, request,redirect, send_file
import matplotlib.pyplot as plt
import sqlite3
import random
import os
import datetime
from flask_sqlalchemy import SQLAlchemy
# 5863127305:AAFthUqUtsUyd0ebqrva7eug-NMmLwChk_A
# -1001849127174 id  del chat
import requests
import numpy as np
import json

app = Flask(__name__)
#Configuración de base de datos


@app.route('/')
def hello_world():  # put application's code here
    conn = sqlite3.connect('base_de_datos.db')
    #conn.execute('''CREATE TABLE cuentas (id INTEGER PRIMARY KEY, periodo INT, precio REAL)''')
    #conn.execute("INSERT INTO cuentas(periodo, precio) VALUES (2, 555.44)")
    #conn.commit()
    datos = conn.execute("SELECT * FROM  cuentas").fetchall()
    # for dato in datos:
    #     print(dato)
    # conn.close()
    # print(datos)
    # Renderiza la plantilla y le pasa la tabla
    return render_template('pantallaExitosa.html',datos=datos)

@app.route('/input_data', methods=["POST", "GET"])
def inputData(): # Ingresar los  datos
    conn = sqlite3.connect('base_de_datos.db')
    datosFormulario = request.form.to_dict()
    print(datosFormulario)
    periodo = datosFormulario['periodo']
    periodo= int(periodo)
    print(type(periodo))
    precio = datosFormulario['precio']
    precio= float(precio)
    #print(precio)
    #print(type(precio))
    query = "INSERT INTO cuentas(periodo,precio) VALUES (?, ?)"
    conn.execute(query, (periodo, precio))
    conn.commit()
    conn.close()
    generarReporte()
    chatBot()
    return redirect('/')

@app.route('/actualizarData', methods=["POST", "GET"] )
def actualizar():
    conn = sqlite3.connect('base_de_datos.db')
    periodos = request.form.getlist('periodo')
    precios = request.form.getlist('precio')

    data= list(zip(periodos, precios))
    print(data)
    conn.execute("DELETE FROM cuentas")
    for per,pre in data:
        per = int(per)
        pre = float(pre)
        query= "INSERT INTO cuentas (periodo,precio) VALUES (?,?)"
        conn.execute(query,(per,pre))
        conn.commit()
    conn.close()
    generarReporte()
    chatBot()
    return redirect('/')


@app.route('/imagen')
def imagen():
    return send_file('diagrama.jpg', mimetype="image/jpg")

@app.route('/eliminar', methods=["POST", "GET"] )
def eliminar():
    atributo = request.form['periodo']
    conn = sqlite3.connect('base_de_datos.db')
    cur = conn.cursor()
    atributo= int(atributo)
    cur.execute("DELETE FROM cuentas WHERE periodo = ?", (atributo,))
    conn.commit()
    cur.close()
    conn.close()
    generarReporte()
    chatBot()
    return redirect('/')


def generarReporte():
    conn = sqlite3.connect('base_de_datos.db')
    datos = conn.execute("SELECT * FROM  cuentas").fetchall()
    x = [resultado[1] for resultado in datos]
    y = [resultado[2] for resultado in datos]
    plt.bar(x, y)
    plt.figure(figsize=(8, 10))
    plt.rcParams['figure.facecolor'] = '#cccccc'
    plt.title("INGRESOS POR AÑO")
    plt.xlabel("PERIODO")
    plt.ylabel("INGRESO")
    fecha_hora = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    colores = ["red", "green", "blue", "orange", "purple"]
    colores_aleatorios = [random.choice(colores) for _ in range(len(x))]
    # Dibujar el diagrama de barras con líneas que unen las puntas de las barras
    pendiente, intercepto = np.polyfit(x, y, 1)
    predicciones = [pendiente * i + intercepto for i in x]
    plt.bar(x, y, color=colores_aleatorios)
    plt.plot(x, predicciones, 'r')
    plt.suptitle('Cliente :Banco Pichincha \n Entorno: Test \n Asunto: Reportes \n Ultima modificación: {} '.format(fecha_hora), y=1, x= 0.3)
    plt.subplots_adjust(top=0.8)
    if os.path.exists('diagrama.jpg'):
        os.remove('diagrama.jpg')
    plt.savefig('diagrama.jpg')
    #plt.show()

    return print("Grafico realizasdo con exito")







# @app.route('/agregar_tarea', methods=['POST'])
# def agregar_tarea():

@app.route('/chatBot')
def chatBot():
    return render_template('pantallaExitosa.html')

#funciones y controladores
def chatBot():
    # Tu token de bot
    bot_token = "5863127305:AAFthUqUtsUyd0ebqrva7eug-NMmLwChk_A"
    # La ID del chat donde quieres enviar el mensaje
    chat_id = "-1001849127174"
    # El mensaje que quieres enviar
    pathImg = "diagrama.jpg"
    # Crea el URL para enviar el mensaje
    url = "https://api.telegram.org/bot{}/sendPhoto".format(bot_token)
    # Crea el objeto JSON con los datos del mensaje
    data = {"chat_id": chat_id}
    img = {"photo": open('diagrama.jpg', "rb")}
    # Envía el mensaje
    response = requests.post(url, files=img, data=data)

    # Verifica que el envío haya sido exitoso
    if response.status_code != 200:
        raise Exception("Error al enviar el mensaje: {}".format(response.status_code))
    return print("Mensaje enviado con éxito!")



if __name__ == '__main__':
    chatBot()
    #app.run()
